## What is it  
 SmartKlik AQT (Advanced Query Tool) is a GUI-based utility that brings you the power of SQL to the excel land in a convenient type-and-run GUI.  

## What Benefits does this tool brings
 It provides a faster way (if not the fastest) to structure or display your excel data in a way suitable for analysis.   
 All you need is SQL.  Common SQL clauses such as GROUP BY, JOIN, WHERE etc are supported too.

## How it looks like  
 Use Advanced Query Tool (AQT) before? What about Toad For Oracle? If yes, then I'm sure you will find this utility alike your hometown.   
 Even if you have not used them before, no worry, with just a few hand-on, you will yourself a familiar land in no time.
 
## What I need to know to use this  
 SQL

## Why is this utility called smartklik AQT?  
 Smartklik is just my trademark for all software I created. AQT is to remind everyone that the usage of this utility is similar to a popular database query tool called **A**dvanced **Q**uery **T**ool.
 Well, I can call it Smartklik Toad (named after yet another popular database query tool), but I find it sound weirds when pronouncing it.
 

## Version  
=======  
1.00  -  First Official Release!  